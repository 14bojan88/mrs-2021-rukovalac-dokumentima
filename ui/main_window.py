from PySide2 import QtWidgets, QtGui
dirPath = r'C:\Skola Treca godina\mrs-2021-rukovalac-dokumentima'

class MainWindow(QtWidgets.QMainWindow):
    def __init__(self, title, dir_path, icon, parent=None):
        super().__init__(parent)
        self.setWindowTitle(title)
        self.setWindowIcon(icon)
        self.resize(800, 600)

        #meni
        self.menu_bar = QtWidgets.QMenuBar(self)
        #model
        self.model = QtWidgets.QFileSystemModel()
        self.model.setRootPath(dir_path)
        #toolbar
        self.tool_bar = QtWidgets.QToolBar("Toolbar",self)
        #statusbar
        self.status_bar = QtWidgets.QStatusBar(self)
        #centralwidget
        self.central_widget = QtWidgets.QStackedWidget(self)
        self.stack1 = QtWidgets.QWidget()
        
        self.Stack1UI()
        
        self.central_widget.addWidget(self.stack1)
        
        #Layout za main window
        hbox = QtWidgets.QHBoxLayout(self)
        hbox.addWidget(self.central_widget)

        self.setLayout(hbox)
        self.show()
        #
        
        self.document = None # otvoreni dokument (trenutno aktivni dokument)

        self.actions_dict = {
            # FIXME: ispraviti ikonicu na X
            "quit": QtWidgets.QAction(QtGui.QIcon("resources/icons/document.png"), "&Quit", self)
            # TODO: dodati i ostale akcije za help i za npr. osnovno za dokument
            # dodati open...
        }

        self._bind_actions()

        self._populate_menu_bar()

        self._populate_tool_bar()

        self.setMenuBar(self.menu_bar)
        self.addToolBar(self.tool_bar)
        self.setStatusBar(self.status_bar)
        self.setCentralWidget(self.central_widget)

    def Stack1UI(self):
        layout = QtWidgets.QHBoxLayout()

        #treeview
        tree_view = QtWidgets.QTreeView()
        tree_view.setModel(self.model)
        tree_view.setRootIndex(self.model.index(dirPath))
        tree_view.setColumnWidth(0, 250)
        tree_view.setAlternatingRowColors(True)
        #TextEdit
        text_edit = QtWidgets.QTextEdit("Tekst edit", None)
        text_edit.setGeometry(100,100,1030,800)

        #Dodavanje 
        layout.addWidget(tree_view)
        layout.addWidget(text_edit)
        
        self.stack1.setLayout(layout)
    

    def _populate_menu_bar(self):
        file_menu = QtWidgets.QMenu("&File")
        edit_menu = QtWidgets.QMenu("&Edit")
        view_menu = QtWidgets.QMenu("&View")
        help_menu = QtWidgets.QMenu("&Help")

        file_menu.addAction(self.actions_dict["quit"])
        edit_menu.addAction(self.actions_dict["quit"])
        view_menu.addAction(self.actions_dict["quit"])
        help_menu.addAction(self.actions_dict["quit"])

    
        self.menu_bar.addMenu(file_menu)
        self.menu_bar.addMenu(edit_menu)
        self.menu_bar.addMenu(view_menu)
        self.menu_bar.addMenu(help_menu)

    def _populate_tool_bar(self):
        dodavanje = QtWidgets.QAction(QtGui.QIcon("resources/icons/add.png"),"add", self)
        self.tool_bar.addAction(dodavanje)
        brisanje = QtWidgets.QAction(QtGui.QIcon("resources/icons/Delete.png"),"add", self)
        self.tool_bar.addAction(brisanje)


    def _bind_actions(self):
        self.actions_dict["quit"].setShortcut("Ctrl+Q")
        self.actions_dict["quit"].triggered.connect(self.close)

    def add_widget(self, widget):
        """
        Adds widget to central (stack) widget
        """
        self.central_widget.addWidget(widget)

    def remove_widget(self, widget):
        self.central_widget.removeWidget(widget)

    # TODO: dodati metodu koja ce u fokus staviti trenutko aktivni plugin (njegov widget)