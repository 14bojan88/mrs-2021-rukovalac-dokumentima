import sys
from PySide2 import QtWidgets, QtGui
from plugin_framework.plugin_registry import PluginRegistry
from ui.main_window import MainWindow


if __name__ == "__main__":
    # TODO: aplikacija
    application = QtWidgets.QApplication(sys.argv)
    dirPath = r'C:\Skola Treca godina\mrs-2021-rukovalac-dokumentima'

    # TODO: main window
    main_window = MainWindow("Rukovalac dokumentima",dirPath, QtGui.QIcon("resources/icons/document.png"))
    plugin_registry = PluginRegistry("plugins", main_window)

    main_window.show()
    sys.exit(application.exec_())
